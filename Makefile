# toolchain
CC = arm-none-eabi-gcc
AS = arm-none-eabi-as
OBGDUMP = arm-none-eabi-objdump
GDB = gdb-multiarch
OPENOCD = openocd

# flags
ASFLAGS = -gstabs
LDFLAGS = -T$(LINKSCR) -nostartfiles -Lgcc -L.
CCFLAGS = -mthumb -mcpu=cortex-m3 -march=armv7-m

# objects
OBJS = main.o startup.o init.o
EXE = blink.elf
LINKSCR = blink.ld
OCD_CONF = openocd.cfg


$(EXE): $(OBJS) $(LINKSCR)
	$(CC) $(CCFLAGS) $(LDFLAGS) -o $(EXE) $(OBJS)

%.o: %.s
	$(AS) $(ASFLAGS) $< -o $@

%.o: %.c
	$(CC) $(CCFLAGS) -c $< -o $@

headers:
	$(OBGDUMP) -h $(EXE) | less

disassemble:
	$(OBGDUMP) -wD $(EXE) | less

gdb:
	$(GDB) -ex 'set architecture armv5' \
	-ex 'target remote | $(OPENOCD) -f $(OCD_CONF)' $(EXE)

clean:
	rm -f $(OBJS)
	rm -f $(EXE)
	rm -f *.log
	rm -f *~

.PHONY: clean gdb disassemble headers
