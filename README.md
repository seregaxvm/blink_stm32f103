## About

This repository contains a bare-metal ARM project for STM32F103 chip. It is build with GNU tool chain whit use of openocd debugger (stlink interface) and is kept is simple is possible. Four branches in this repository contain four different versions of `blink` project:

- `ram/assembly` is the simplest. It doesn't use flash memory and is loaded directly in RAM.
- `flash/assembly` uses flash to store `.text` section and initial values of `.data` section.
- `flash/assembly2` moves `.text` section to RAM before executing the program.
- `flash/c` initialises stack in assembly and uses C language for the rest.

*Note: STM32F103 boot process is controlled via boot pins (usually wired to jumpers). Consult with data-sheet to learn how to switch boot modes*

## Install tool-chain

To install tool-chain on Debian system, issue this command:

```sudo apt-get install gcc-arm-none-eabi gdb-multiarch openocd```

## Run

`Makefile` contain recipes for building the project.

- `make` to compile the project
- `make disassemble` to show the disassembly of the binary
- `make headers` to show the section summary
- `make gdb` to run openocd debugger and connect gdb to it

## Loading image

If connected to openocd with gdb, all following commands should be prepended with `monitor`

To stop the processor issue the command `reset halt`

To write image into RAM issue the command `load_image blink.elf`

To write image to Flash, unprotect it: `flash protect 0 0 127 off`

*Note: command arguments may differ. Consult with openocd manual to learn about Flash banks and sectors*

Then, erase it: `flash erase_sector 0 0 127`

Then, write the image: `flash write_image blink.elf`

To restart the processor issue the command `reset run`
