#include<stdio.h>
#include"io.h"

void low_level_init(void) {
    const uint32_t GPIOCDD = 0x44144444;
    const uint32_t LEDON = (1 << 13);
    const uint32_t LEDOFF = 0;

    *(uint32_t volatile *)((uint32_t) RCC_BASE + (uint32_t) RCC_AHB2ENR) |=
        IOPCEN;
    *(uint32_t volatile *)((uint32_t) GPIO_C + (uint32_t) GPIOC_CRH) = GPIOCDD;
}
