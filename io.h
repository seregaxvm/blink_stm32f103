#pragma once

#define FLASH_BASE 0x8000000
#define RAM_BASE 0x20000000
#define SYSTEM_MEMORY 0x1ffff000
#define PERIPHERALS 0x40000000

#define GPIO_C 0x40011000
#define GPIOC_CRH 0x4
#define GPIOC_ODR 0xc

#define RCC_BASE 0x40021000
#define RCC_AHB2ENR 0x18
#define IOPCEN (1<<4)
