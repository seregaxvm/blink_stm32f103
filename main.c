#include<stdio.h>
#include"io.h"

void sleep_cycle(uint32_t cycles) {
    for (int i = 0; i < cycles; i++) {
    }
}

void main(void) {
    uint32_t sleep = 1000000;

    while (1) {
        *(uint32_t volatile *)((uint32_t) GPIO_C + (uint32_t) GPIOC_ODR) = 0;
        sleep_cycle(sleep);
        *(uint32_t volatile *)((uint32_t) GPIO_C + (uint32_t) GPIOC_ODR) =
            (1 << 13);
        sleep_cycle(sleep);
    }
}
